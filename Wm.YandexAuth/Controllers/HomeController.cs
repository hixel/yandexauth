﻿namespace Wm.YandexAuth.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Web;
    using System.Web.Mvc;
    using DotNetOpenAuth.OAuth.Messages;
    using Microsoft.Ajax.Utilities;
    using Models.AuthWithYandex;
    using DotNetOpenAuth.Messaging;
    using DotNetOpenAuth.OpenId.RelyingParty;
    using Newtonsoft.Json;
    using NLog;
    using SharpRaven;
    using SharpRaven.Data;
    using SharpRaven.Utilities;

    public class HomeController : Controller
    {
        public static readonly OpenIdRelyingParty OpenIdRelyingParty = new OpenIdRelyingParty();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AuthWithYandexOpenId()
        {
            var response = OpenIdRelyingParty.GetResponse();

            var model = new AuthOpenIdModel();

            if (response == null)
            {
                // Пользователь не аутентифицирован
                var request = OpenIdRelyingParty.CreateRequest("http://openid.yandex.ru/");
                return request.RedirectingResponse.AsActionResultMvc5();
            }

            model.AuthenticationStatus = response.Status;
            switch (response.Status)
            {
                case AuthenticationStatus.Authenticated:
                    var idParts = response.ClaimedIdentifier.ToString().Split(new [] {"/"}, 
                        StringSplitOptions.RemoveEmptyEntries);

                    model.Message = string.Format("Пользователь {0} успешно аутентифицирован",
                        idParts[idParts.Length - 1]);
                    break;

                case AuthenticationStatus.Canceled:
                    model.Message = "Пользователь {0} отказался от аутентификации";
                    break;

                case AuthenticationStatus.Failed:
                    model.Message = "Аутентификация пользователя не удалась";
                    break;

                default:
                    model.Message = "Неизвестная ошибка";
                    break;
            }

            return View(model);
        }

        public ActionResult Log(string message)
        {
            var client = new RavenClient("http://07db47e4285e4d7c98aeb3ccf310cc62:079345ec61bd4deb8d3c56cedb0f45ea@172.16.29.35:9000/2");
            client.ErrorOnCapture = exception =>
            {
                Console.WriteLine(exception.ToString());
            };
            var result = client.CaptureException(new Exception("LOG_TEST"));
            result = client.CaptureMessage("LOG_TEST");

            return Redirect("/");
        }

        public ActionResult AuthWithYandexPassport(string code)
        {
            var model = new AuthPassportModel
            {
                Success = false, 
                Message = string.Empty
            };

            var authToken = GetAuthToken(code);
            if (string.IsNullOrEmpty(authToken))
            {
                model.Message = "Пользователь не найден";

                return View(model);
            }

            var yandexPassport = AuthorizeToken(authToken);
            if (yandexPassport == null)
            {
                model.Message = "Проблемы при получении паспорта пользователя";

                return View(model);
            }

            model.Success = true;
            AutoMapper.Mapper.CreateMap<YandexPassportInfo, YandexPassportViewModel>()
                .ForMember(x => x.Email, y => y.MapFrom(x => x.DefaultEmail))
                .ForMember(x => x.OpenIdList, y => y.MapFrom(x => x.OpenIdIdentities.ToList()));
            model.YandexPassport = AutoMapper.Mapper.Map<YandexPassportViewModel>(yandexPassport);

            return View(model);
        }

        private string GetAuthToken(string code)
        {
            var data = string.Format("grant_type=authorization_code&code={0}&client_id={1}&client_secret={2}",
                code,
                "ed0530fd4de54127a61a4a3088bf3a38",
                "c915ecac9b8b467f83a92c4a88a718cb");

            var request = WebRequest.Create("https://oauth.yandex.ru/token");
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            try
            {
                using (var requestStream = request.GetRequestStream())
                {
                    var byteArr = Encoding.UTF8.GetBytes(data);
                    requestStream.Write(byteArr, 0, byteArr.Length);
                }

                var response = request.GetResponse();
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return string.Empty;
                    }
                    var reponseStatus = ((HttpWebResponse)response).StatusCode;

                    if (reponseStatus != HttpStatusCode.OK)
                    {
                        return string.Empty;
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        var responseText = reader.ReadToEnd();

                        var yandexToken = JsonConvert.DeserializeObject<YandexToken>(responseText);

                        return yandexToken.AccessToken;
                    }
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }    
        }

        private YandexPassportInfo AuthorizeToken(string authToken)
        {
            var uriBuilder = new UriBuilder("https://login.yandex.ru/info");
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters["oauth_token"] = authToken;
            parameters["format"] = "json";
            parameters["with_openid_identity"] = "yes";
            uriBuilder.Query = parameters.ToString();

            var request = WebRequest.Create(uriBuilder.Uri);
            request.Method = "GET";

            try
            {
                var response = request.GetResponse();
                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                    {
                        return null;
                    }
                    var reponseStatus = ((HttpWebResponse)response).StatusCode;

                    if (reponseStatus != HttpStatusCode.OK)
                    {
                        return null;
                    }

                    using (var reader = new StreamReader(stream))
                    {
                        var responseText = reader.ReadToEnd();

                        var yandexPassport = JsonConvert.DeserializeObject<YandexPassportInfo>(responseText);

                        return yandexPassport;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal class YandexToken
        {
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }

            [JsonProperty("token_type")]
            public string TokenType { get; set; }

            [JsonProperty("expires_in")]
            public int ExpiresIn { get; set; }
        }

        internal class YandexPassportInfo
        {
            [JsonProperty("id")]
            public long Id { get; set; }

            [JsonProperty("login")]
            public string Login { get; set; }

            [JsonProperty("sex")]
            public string Sex { get; set; }

            [JsonProperty("first_name")]
            public string FirstName { get; set; }

            [JsonProperty("last_name")]
            public string LastName { get; set; }

            [JsonProperty("display_name")]
            public string DisplayName { get; set; }

            [JsonProperty("real_name")]
            public string RealName { get; set; }

            [JsonProperty("emails")]
            public string[] Emails { get; set; }

            [JsonProperty("openid_identities")]
            public string[] OpenIdIdentities { get; set; }

            [JsonProperty("default_email")]
            public string DefaultEmail { get; set; }
        }
    }
}