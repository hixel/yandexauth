﻿namespace Wm.YandexAuth.Models.AuthWithYandex
{
    using DotNetOpenAuth.OpenId.RelyingParty;

    public class AuthOpenIdModel
    {
        public AuthenticationStatus AuthenticationStatus { get; set; }

        public string Message { get; set; }
    }
}