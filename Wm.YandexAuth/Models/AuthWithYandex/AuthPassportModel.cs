﻿namespace Wm.YandexAuth.Models.AuthWithYandex
{
    using System.Collections.Generic;

    public class AuthPassportModel
    {
        public bool Success { get; set; }

        public YandexPassportViewModel YandexPassport { get; set; }
 
        public string Message { get; set; }
    }

    public class YandexPassportViewModel
    {
        public string Login { get; set; }

        public string Email { get; set; }

        public IList<string> OpenIdList { get; set; }
    }
}